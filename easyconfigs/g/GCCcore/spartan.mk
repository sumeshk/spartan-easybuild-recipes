# This BUILD_CONFIG option builds checks that toggling debug
# information generation doesn't affect the generated object code.

# It is very lightweight: in addition to not performing any additional
# compilation (unlike bootstrap-debug-lean), it actually speeds up
# stage2, for no debug information is generated when compiling with
# the unoptimized stage1.

# For more thorough testing, see bootstrap-debug-lean.mk
TFLAGS += -mno-avx512f -mno-avx512pf -mno-avx512er -mno-avx512cd
STAGE1_TFLAGS += -mno-avx512f -mno-avx512pf -mno-avx512er -mno-avx512cd
STAGE2_TFLAGS += -mno-avx512f -mno-avx512pf -mno-avx512er -mno-avx512cd
STAGE3_TFLAGS += -mno-avx512f -mno-avx512pf -mno-avx512er -mno-avx512cd
CFLAGS += -mno-avx512f -mno-avx512pf -mno-avx512er -mno-avx512cd
CXXFLAGS += -mno-avx512f -mno-avx512pf -mno-avx512er -mno-avx512cd
CPPFLAGS += -mno-avx512f -mno-avx512pf -mno-avx512er -mno-avx512cd
BOOT_CFLAGS += -mno-avx512f -mno-avx512pf -mno-avx512er -mno-avx512cd
STAGE1_CFLAGS += -mno-avx512f -mno-avx512pf -mno-avx512er -mno-avx512cd
STAGE2_CFLAGS += -mno-avx512f -mno-avx512pf -mno-avx512er -mno-avx512cd
STAGE3_CFLAGS += -mno-avx512f -mno-avx512pf -mno-avx512er -mno-avx512cd
STAGE4_CFALGS += -mno-avx512f -mno-avx512pf -mno-avx512er -mno-avx512cd
CFLAGS_FOR_TARGET += -mno-avx512f -mno-avx512pf -mno-avx512er -mno-avx512cd
CXXFLAGS_FOR_TARGET += -mno-avx512f -mno-avx512pf -mno-avx512er -mno-avx512cd
GOCFLAGS_FOR_TARGET += -mno-avx512f -mno-avx512pf -mno-avx512er -mno-avx512cd
STAGEprofile_CFLAGS += -mno-avx512f -mno-avx512pf -mno-avx512er -mno-avx512cd
STAGEtrain_CFLAGS += -mno-avx512f -mno-avx512pf -mno-avx512er -mno-avx512cd
STAGEfeedback_CFLAGS += -mno-avx512f -mno-avx512pf -mno-avx512er -mno-avx512cd
#STAGE2_CONFIGURE_FLAGS += -mno-avx512f -mno-avx512pf -mno-avx512er -mno-avx512cd
#STAGE_CONFIGURE_FLAGS += -mno-avx512f -mno-avx512pf -mno-avx512er -mno-avx512cd

